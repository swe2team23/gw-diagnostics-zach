# make a fatass list of 100,000 ints of a random number
# check to see if all of those individual ints are equal to the random number generated
# if one is not equal send fail and tell which index not generated
import sys
from random import randint

listSize = 1000000
match = True
random = randint(1,101)
listOfWrongIndices = []


list = []

for i in range(listSize):
    list.append(random)

for i in range(listSize):
    if(list[i] != random):
        listOfWrongIndices.append(i)

if(len(listOfWrongIndices) >= 1):
    match = False
    print("memory test FAIL!", file=sys.stderr)
else:
    print("memory test PASS")

