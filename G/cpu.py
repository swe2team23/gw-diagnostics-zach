import sys

#INT TEST FUNCTIONS
def intInitialTest():
    initial = 22
    return initial

def intAdditionTest():
    addition = 2+2
    return addition

def intMultiplicationTest():
    multiply = 2*2
    return multiply

def intSubtractionTest():
    subtract = 2-2
    return subtract

def intDivisionTest():
    divide = 2/2
    return divide

def intModTest():
    mod = 5%2
    return mod


#FLOAT TEST FUNCTIONS
def floatInitialTest():
    initial = 2.55
    return initial

def floatAdditionTest():
    addition = 2.00 + .22
    return addition

def floatMultiplicationTest():
    multiply = 2.22 * 1.00
    return multiply

def floatSubtractionTest():
    subtract = 4.44 - 2.22
    return subtract

def floatDivisionTest():
    divide = 2.22 / 2
    return divide

def floatModTest():
    mod = 5.0%2.0
    return mod



#INT TESTS / OUTPUT
if(intInitialTest() == 22):
    print('int initialization PASS')
else:
    print("int initialization FAIL!", file=sys.stderr)

if(intAdditionTest() == 4):
    print('int addition PASS')
else:
    print("int addition FAIL!", file=sys.stderr)

if(intMultiplicationTest() == 4):
    print('int multiplication PASS')
else:
    print("int multiplication FAIL!", file=sys.stderr)

if(intSubtractionTest() == 0):
    print('int subtraction PASS')
else:
    print("int subtraction FAIL!", file=sys.stderr)

if(intDivisionTest() == 1):
    print('int division PASS')
else:
    print("int division FAIL!", file=sys.stderr)

if(intModTest() == 1):
    print('int mod PASS')
else:
    print("int mod FAIL!", file=sys.stderr)



#FLOAT TESTS / OUTPUT
if(floatInitialTest() == 2.55):
    print('float initialization PASS')
else:
    print("float initialization FAIL!", file=sys.stderr)

if(floatAdditionTest() == 2.22):
    print('float addition PASS')
else:
    print("float addition FAIL!", file=sys.stderr)

if(floatMultiplicationTest() == 2.22):
    print('float multiplication PASS')
else:
    print("float multiplication FAIL!", file=sys.stderr)

if(floatSubtractionTest() == 2.22):
    print('float subtraction PASS')
else:
    print("float subtraction FAIL!", file=sys.stderr)

if(floatDivisionTest() == 1.11):
    print('float division PASS')
else:
    print("float division FAIL!", file=sys.stderr)

if(floatModTest() == 1.0):
    print('float mod PASS')
else:
    print("float mod FAIL!", file=sys.stderr)
